# AzureDeploymentHistoryParses


## Description
Azure deployment history parser that parses the Parameters and Output entities of the deployment using `Get-AzResourceGroupDeployment` and saves them in CSV format for further analysis.

## What is a Deployment History
Deployment History is a record of all the deployments made to a specific resource group. It includes information about who made the deployment, when it was made, the deployment status, and any other information that was captured during the deployment.

Azure keeps a history of all deployments so that you can track changes to your resources over time. This can be helpful for auditing purposes, as well as for troubleshooting issues that may arise after a deployment.

## Juicy things found in Deployment History
Some forgotten keys and secrets in the output.

## How to run
> Get-AzureDeploymentHistory.ps1

Note:
There might be errors while running but dont worry, that's because of null data in some deployments. Scripts works as intended just needed to clean up the error messages later.

