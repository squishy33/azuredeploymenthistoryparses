# Deployment History Parser, gives you a CSV output in C:\Temp\DeploymentHistory
# Parses to all the Parameters and Outputs of your Deployment History. Manually found in RG > Under Settings "Depoyments"
# 
#
# Pre-requisite: Az module or Az.Resources
# > Import-Module -Name Az
#
#  To run:
# > Get-AzureDeploymentHistory.ps1
# the rest of the step should be straigh forward
#

# Connect to Azure
$TenantID = Read-Host -Prompt "Tenant Id: " 
Connect-AzAccount -tenant $TenantID

# Choose target subscription
$subID = Get-AzSubscription | Select-Object -Property "Name", "Id" | out-gridview -Title "Choose Subscription" -outputmode single
#end choosing subscription

# Set target subscription
Set-AzContext -Subscription $subID.Id

# Checks if output path is available and making sure everytime you run it it cleans up the DeploymentHistory
# Make sure to save the files before running it again

$path = "C:\temp\DeploymentHistory"
if(!(test-path $path))
{       
    Write-Host "Creating output folder...." -ForeGroundColor Green
    New-Item -ItemType Directory -Path $path
    New-Item -ItemType Directory -Path "$path\Input"
    New-Item -ItemType Directory -Path "$path\Output"
    New-Item -ItemType Directory -Path "$path\Error"
}else {
    Write-Host "Recreating the output folder path....." -ForeGroundColor Green
    Remove-Item -path $path -Force -Recurse
    New-Item -ItemType Directory -Path $path
    New-Item -ItemType Directory -Path "$path\Input"
    New-Item -ItemType Directory -Path "$path\Output"
    New-Item -ItemType Directory -Path "$path\Error"
}

Write-Host "Fetching deployment history data...." -ForeGroundColor Green
$ResourceGroupNames = (Get-AzResourceGroup).ResourceGroupName
foreach ($ResourceGroup in $ResourceGroupNames) {

    $DeploymentHistory = Get-AzResourceGroupDeployment -ResourceGroupName  $ResourceGroup
    $DeploymentHistoryResultOutput = @()
    $DeploymentHistoryResultInput = @()
    
    foreach ($Outputs in $DeploymentHistory) {
        try {
            $DeploymentName = $Outputs.DeploymentName
            $TimeStamp = $Outputs.Timestamp
            $Mode = $Outputs.Mode
            
            foreach ($output in $Outputs.Outputs.GetEnumerator()) {
                $OutputTitle = $output.Key
                $OutputValue = $output.Value.ToString()
                $DeploymentHistoryResultOutput += [pscustomobject]@{
                    DeploymentNameOutput = $DeploymentName
                    OutputTitle = $OutputTitle
                    OutputValue = $OutputValue
                    Timstamp = $TimeStamp
                    Mode = $Mode
                }
            }
            foreach ($output in $Outputs.Parameters.GetEnumerator()) {
                $InputTitle = $output.Key
                $InputValue = $output.Value.ToString()
                $DeploymentHistoryResultInput += [pscustomobject]@{
                    DeploymentNameInput = $DeploymentName
                    InputTitle = $InputTitle
                    InputValue = $InputValue
                    Timstamp = $TimeStamp
                    Mode = $Mode
                }
            }
        }
        catch {
            Write-Output "Error Message: $_.Exception.Message" >> $path\Error\ErrorMessage.txt
        }
    }
    $DeploymentHistoryResultOutput | Select-Object "DeploymentNameOutput", "Timstamp", "Mode", "OutputTitle", "OutputValue" | Export-Csv -NoTypeInformation -Path $path\Output\$ResourceGroup.csv
    $DeploymentHistoryResultInput | Select-Object "DeploymentNameInput", "Timstamp", "Mode", "InputTitle", "InputValue" | Export-Csv -NoTypeInformation -Path $path\Input\$ResourceGroup.csv
    
}

# disconnecting
Write-Host "Exiting....If error, run disconnect-azaccount again" -ForeGroundColor Green
disconnect-azaccount

# Open File Path
ii $path
# END